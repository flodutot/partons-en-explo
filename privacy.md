# Privacy Statement - Règle de confidentialité
## Français:
_**La plus grande politique de confidentialité du monde**_

Nous ne stockons pas vos données, point final.

Nous ne pouvons pas physiquement. Nous n'avons nulle part où le stocker. Nous n'avons même pas de base de données sur un serveur pour stocker d'éventuelles informations.
Donc, même si Justin Bieber demandait gentiment de voir vos données, nous n'aurions rien à lui montrer.

Aigle Digital



## English:
_**The World's Greatest Privacy policy**_

We don't store your data, period.

We physically can't. We have nowhere to store it. We don't even have a sever database to store it.
So, even if Justin Bieber asked nicely to see your data, we would'nt have anything to show him.

Aigle Digital